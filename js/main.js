$(document).ready(function() {

    /*$(window).scroll(function() {
         var p = $('.navlogo').position();
         if ($(window).scrollTop() < p.top - 150) {
             $('body').removeClass('move-logo');
             $('.navlogo img').attr('src', 'img/gen1logo.png');
         } else if ($(window).scrollTop() > p.top - 250) {
             $('body').addClass('move-logo');
             $('.navlogo img').attr('src', 'img/logonav.png');
         }

     })*/
    if ($(window).width() >= 1200) {
        var imageHeight = parseInt($('.navlogo img').css('height')),
            stopHeight = imageHeight / 2;
	var changelogo = 0;


        /*marginHeight = parseInt($('.navbar').css('margin-top'))
        stopMargin = marginHeight / 2;*/
        if ($('.navlogo img').attr('src') != 'img/logonav.png') {
            $(window).scroll(function(e) {
                var windowScroll = $(window).scrollTop(),
                    imageWidth = parseInt($('.navlogo img').css('width')),
                    p = $('.navlogo').position();
                /* newMargin = marginHeight - windowScroll,*/
                // console.log(p.top);
               // console.log($(window).height() / 2);
                if (windowScroll >= p.top - $(window).height() / 2) {
                    newHeight = imageHeight - 0.8 * (windowScroll - p.top + $(window).height() / 2);
              //      console.log(windowScroll);

                    if (newHeight >= stopHeight) {
                        $('.navlogo img').css({
                            "height": newHeight
                        });
                        $('.navlogo').css("left", 'calc(50% - ' + newHeight * 500 / 353 / 2 + 'px)');
                    } else {
                        $('.navlogo').css({
                            'position': 'fixed',
                            'top': 0,
                            marginTop: 60,
				'left': 'calc(50% - 125px)',
                        });
			
			if (changelogo == 0) {
			$('.navlogo img').fadeOut(20, function() {
        $('.navlogo img').attr("src","img/logonav.png");
        $('.navlogo img').fadeIn(400);
			
    });
			changelogo = 1;
			}
                       // $('.navlogo img').attr('src', 'img/logonav.png');

                        /*.css({
                                                width: '90%',
                                                height: '80%',
                                                marginLeft: '5%'
                                            })*/
                    }
                }

            });
        }
    } else {
        $('.navlogo img').attr('src', 'img/logonav.png').css({
            width: 'auto',
            height: '73px',
            position: 'fixed',
            top: '6px',
            right: 0
        })
    }


    /*SLIDER*/
    var width = 100;
    var animation_speed = 1200;
    var time_val = 5000;
    var current_slide = 1;

    var $slider = $('#slider');
    var $slide_container = $('#slider ul');
    var $slides = $('#slider li');

    var interval;

    $slides.each(function(index) {
        $(this).css('left', (index * 100) + '%');
    });

    function slider() {
        $slide_container.animate({
            'left': '-=' + (width + '%')
        }, animation_speed, function() {
            if (++current_slide === $slides.length) {
                current_slide = 1;
                $slide_container.css('left', 0);
            }
        });
    }

    interval = setInterval(slider, time_val);

    $slide_container.hover(function() {
        clearInterval(interval);
    }, function() {
        interval = setInterval(slider, time_val);
    })
});
